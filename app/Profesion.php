<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesion extends Model
{
    protected $fillable = [
        'nombre'
    ];

    protected $table = "profesions";

    protected $primarykey = "id";

    public function docente()
    {
        return $this->hasMany(Docente::class);
    }
}
