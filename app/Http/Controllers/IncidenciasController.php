<?php

namespace App\Http\Controllers;

use App\Incidencias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IncidenciasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $iduser = Auth::user()->id;

        $incidencias = Incidencias::where('user_id', $iduser)->get();
        
        return view('Incidencias.index', compact('incidencias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Incidencias  $incidencias
     * @return \Illuminate\Http\Response
     */
    public function show(Incidencias $incidencias)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Incidencias  $incidencias
     * @return \Illuminate\Http\Response
     */
    public function edit(Incidencias $incidencias)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Incidencias  $incidencias
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Incidencias $incidencias)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Incidencias  $incidencias
     * @return \Illuminate\Http\Response
     */
    public function destroy(Incidencias $incidencias)
    {
        //
    }
}
