<?php

namespace App\Http\Controllers;

use App\IncidenciaDetalle;
use Illuminate\Http\Request;

class IncidenciaDetalleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IncidenciaDetalle  $incidenciaDetalle
     * @return \Illuminate\Http\Response
     */
    public function show(IncidenciaDetalle $incidenciaDetalle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IncidenciaDetalle  $incidenciaDetalle
     * @return \Illuminate\Http\Response
     */
    public function edit(IncidenciaDetalle $incidenciaDetalle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IncidenciaDetalle  $incidenciaDetalle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IncidenciaDetalle $incidenciaDetalle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IncidenciaDetalle  $incidenciaDetalle
     * @return \Illuminate\Http\Response
     */
    public function destroy(IncidenciaDetalle $incidenciaDetalle)
    {
        //
    }
}
