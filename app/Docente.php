<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Docente extends Model
{
    protected $fillable = [
        'user_id', 'nombre', 'apellidos', 'correo', 'carnet', 'profesion_id', 'carrera_id'
    ];

    protected $table = "docentes";

    protected $primarykey = "id";

    public function nombrecompleto()
    {
        return $this->nombre . ' ' . $this->apellidos;
    }

    public function profesion()
    {
        return $this->belongsTo(Profesion::class);
    }

    public function carrera()
    {
        return $this->belongsTo(Carrera::class);
    }
}
