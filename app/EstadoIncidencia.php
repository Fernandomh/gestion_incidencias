<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoIncidencia extends Model
{
    protected $fillable = [
        'nombre'
    ];

    protected $table = "estado_incidencias";

    protected $primarykey = "id";

    public function incidencia()
    {
        return $this->hasMany(Incidencias::class);
    }
}
