<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incidencias extends Model
{
    protected $fillable = [
        'user_id', 'user_asignado_id', 'carrera_id', 'titulo', 'descripcion', 'estado_id'
    ];

    protected $table = "incidencias";

    protected $primarykey = "id";

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function estado()
    {
        return $this->belongsTo(EstadoIncidencia::class, 'estado_id');
    }
}
