@extends('layouts.app')

@section('content')
<div class="container">

    <div class="pt-3 pb-3">
        <a class="btn btn-success" data-toggle="tooltip" href="{{ route('registrarEstudiante')}}" title="Nuevo Registro">
            <i class="fas fa-plus">
            </i>
            Nuevo Registro
        </a>
    </div>

    <div class="card">
        <div class="card-header text-center">
            <strong><span style="font-size: 20px;">LISTA DE INCIDENCIAS</span></strong>
        </div>
        <div class="card-body">
            <table id="tabledata" class="table table-bordered table-hover table-sm">
                <thead>
                    <tr>
                        <th class="text-center" width="40px">
                            NRO
                        </th>
                        <th class="text-center">
                            TITULO
                        </th>
                        <th class="text-center">
                            DESCRIPCION
                        </th>
                        <th class="text-center">
                            ESTADO
                        </th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <?php $a = 1; ?>
                <tbody>
                    @foreach($incidencias as $inci)
                    <tr>
                        <td class="text-center"> <?php echo "$a"; ?> </td>
                        <td class="text-center">
                            {{ $inci->titulo}}
                        </td>
                        <td class="text-center">
                            {{ $inci->descripcion}}
                        </td>
                        <td class="text-center">
                            {{ $inci->estado_id}}
                        </td>
                        
                        <td class="text-center" width="5px">
                            <a class="btn btn-primary btn-sm" data-toggle="tooltip" href="{{route('editarEstudiante', $inci->id)}}" title="Editar Estudiante">
                                <i class="bi bi-pencil-square"></i>
                            </a>
                        </td>

                        <td class="text-center" width="5px">
                            <a class="btn btn-warning btn-sm" data-toggle="tooltip" href="{{route('detalleEstudiante', $inci->id)}}" title="Detalles del Estudiante">
                                <i class="bi bi-person-lines-fill"></i>
                            </a>
                        </td>
                    </tr>
                    <?php $a++; ?>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<br>
@endsection