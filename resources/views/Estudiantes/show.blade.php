@extends('layouts.app')

@section('content')

<div class="container" style="width: 650px;">
    <div class="card card-outline card-info">
        <div class="card-header">
            <h3 class="card-title">DETALLES DEL ESTUDIANTE</h3>
        </div>
        <div class="card-body">
            <dl class="row">
                <dt class="col-sm-3">
                    NOMBRE:
                </dt>
                <dd class="col-sm-9">
                    {{ $estudiante->nombre}}
                </dd>
            </dl>
            <div>
               
                <a class="btn btn-warning" data-toggle="tooltip" href="{{route('estudiantes') }}" title="Volver al Listado">Volver</a>
                
                <a class="btn btn-primary" data-toggle="tooltip" href="{{ route('editarEstudiante', $estudiante->id)}}" title="Editar Oficina">
                    <i class="fas fa-edit">
                    </i> Editar
                </a>
               
            </div>
        </div>

    </div>
</div>
@endsection