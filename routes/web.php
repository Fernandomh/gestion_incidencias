<?php


Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::group(['middleware' => ['auth'] ], function () {

    Route::get('/home', 'HomeController@index')->name('home');

    /* --- RUTAS PARA EL ESTUDIANTE --*/
    Route::get('/estudiantes', 'EstudianteController@index')->name('estudiantes');

    Route::get('/estudiantes/create', 'EstudianteController@create')->name('registrarEstudiante');

    Route::post('/post_registrar_estudiante', 'EstudianteController@store')->name('postRegistrarEstudiante');

    Route::get('/estudiantes/edit/{id}', 'EstudianteController@edit')->name('editarEstudiante');

    Route::put('/post_editar_estudiante/{id}', 'EstudianteController@update')->name('postEditarEstudiante');

    Route::get('/estudiante/detalles/{id}', 'EstudianteController@show')->name('detalleEstudiante');


    /* --- RUTAS PARA EL DOCENTE --*/
    Route::get('/docentes', 'DocenteController@index')->name('docentes');

    Route::get('/docentes/create', 'DocenteController@create')->name('registrarDocente');

    Route::post('/post_registrar_docente', 'DocenteController@store')->name('postRegistrarDocente');

    Route::get('/docentes/edit/{id}', 'DocenteController@edit')->name('editarDocente');

    Route::put('/post_editar_docente/{id}', 'DocenteController@update')->name('postEditarDocente');

    Route::get('/docente/detalles/{id}', 'DocenteController@show')->name('detalleDocente');


    /* --- RUTAS PARA LAS INCIDENCIAS --*/
    Route::get('/incidencias', 'IncidenciasController@index')->name('incidencias');

    // Route::get('/docentes/create', 'DocenteController@create')->name('registrarDocente');

    // Route::post('/post_registrar_docente', 'DocenteController@store')->name('postRegistrarDocente');

    // Route::get('/docentes/edit/{id}', 'DocenteController@edit')->name('editarDocente');

    // Route::put('/post_editar_docente/{id}', 'DocenteController@update')->name('postEditarDocente');

    // Route::get('/docente/detalles/{id}', 'DocenteController@show')->name('detalleDocente');
});